SUMMARY = "DUT Manager recipes"
DESCRIPTION = "A tool to control DUTs through interfaces"
LICENSE = "CLOSED"
SECTION = "rws"

SRC_URI = "gitsm://gitlab.com/embedded_linux_factory/rws/dut-manager;protocol=https;branch=main"
SRC_URI += " file://default_config_manager.json "
SRC_URI += " file://remote-workstation.service "
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

FILES:${PN} += " ${systemd_unitdir}/system/remote-workstation.service "
FILES:${PN} += " /etc/rws "
FILES:${PN} += " /usr/bin "

DEPENDS = "  \
    curl     \
    glib-2.0 \
    json-c \
    libmicrohttpd \
    libsocketcan \
    libusbrelay \
"
RDEPENDS:${PN} = "  \
    curl     \
    glib-2.0 \
    json-c \
    libmicrohttpd \
    libsocketcan \
    libusbrelay \
"

SYSTEMD_SERVICE:${PN} = "remote-workstation.service"

do_install:append() {
    # Install unit file
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/remote-workstation.service ${D}${systemd_unitdir}/system

    # Install binary & default configuration
    install -d ${D}${bindir}
    install -m 0755 dut-manager ${D}${bindir}

    install -d ${D}${sysconfdir}/rws
    install -m 0644 ${WORKDIR}/default_config_manager.json ${D}${sysconfdir}/rws/config_manager.json
    install -d ${D}${sysconfdir}/rws/duts_configs
}

inherit meson
inherit pkgconfig
inherit systemd
inherit useradd

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "-M -r -s /bin/false rws"
